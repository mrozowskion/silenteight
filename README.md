App is easily runnable through docker. Image is available on [Docker hub](https://hub.docker.com/r/mrozowski/silent-eight-task)

```
docker pull mrozowski/silent-eight-task
```

