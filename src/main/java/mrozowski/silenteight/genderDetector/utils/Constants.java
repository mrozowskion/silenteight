package mrozowski.silenteight.genderDetector.utils;

public class Constants {
    public final static String FEMALE = "Female";
    public final static String MALE = "Male";
    public final static String INCONCLUSIVE = "INCONCLUSIVE";
}
