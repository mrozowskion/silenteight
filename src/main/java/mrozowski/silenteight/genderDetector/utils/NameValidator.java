package mrozowski.silenteight.genderDetector.utils;

import org.springframework.stereotype.Service;

import java.util.function.Predicate;

@Service
public class NameValidator implements Predicate<String> {
    @Override
    public boolean test(String name) {
        return name.matches("(?i)(^\\p{L})((?![ .,'-]$)[\\p{L} .,'-]){2,24}$");
    }
}
