package mrozowski.silenteight.genderDetector.exception;

import com.sun.jdi.InternalException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {InternalException.class})
    public ResponseEntity<Object> handlerApiRequestException(HttpServletRequest req, InternalException e){
        HttpStatus badRequest = HttpStatus.INTERNAL_SERVER_ERROR;

        ApiException exception = new ApiException(
                ZonedDateTime.now(ZoneId.of("Z")),
                badRequest,
                e.getMessage(),
                req.getRequestURI()
        );
        return new ResponseEntity<>(exception, badRequest);
    }

    @ExceptionHandler(value = {InvalidNameException.class})
    public ResponseEntity<Object> handlerApiRequestException(HttpServletRequest req, InvalidNameException e){
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;

        ApiException exception = new ApiException(
                ZonedDateTime.now(ZoneId.of("Z")),
                badRequest,
                e.getMessage(),
                req.getRequestURI()
        );
        return new ResponseEntity<>(exception, badRequest);
    }
}
