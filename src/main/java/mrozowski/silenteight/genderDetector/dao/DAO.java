package mrozowski.silenteight.genderDetector.dao;

import java.util.List;

public interface DAO {
    boolean isFemaleName(String name);
    boolean isMaleName(String name);

    List<String> getFemaleNames();
    List<String> getMaleNames();
}
