package mrozowski.silenteight.genderDetector.dao;

import com.sun.jdi.InternalException;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Repository("file_db")
public class GenderDetectorDataAccess implements DAO {

    private final String FEMALE_FILE = "/static/female.txt";
    private final String MALE_FILE = "/static/male.txt";

    @Override
    public boolean isFemaleName(String name) {
        return checkName(name, FEMALE_FILE, openFile(FEMALE_FILE));
    }

    @Override
    public boolean isMaleName(String name) {
        return checkName(name, MALE_FILE, openFile(MALE_FILE));
    }

    @Override
    public List<String> getFemaleNames() {
        return getTokens(FEMALE_FILE, openFile(FEMALE_FILE));
    }

    @Override
    public List<String> getMaleNames() {
        return getTokens(MALE_FILE, openFile(MALE_FILE));
    }

    public Reader openFile(String filePath){
        InputStream in = getClass().getResourceAsStream(filePath);
        if(in == null) throw new InternalException(String.format("File %s not found", filePath));
        return new InputStreamReader(in);
    }

    public boolean checkName(String name, String fileName, Reader fileContent){
        String lowerCaseName = name.toLowerCase();

        try (BufferedReader br = new BufferedReader(fileContent)) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.toLowerCase().equals(lowerCaseName))
                    return true;
            }
        } catch (IOException e) {
            throw new InternalException("File Error: " + fileName);
        }
        return false;
    }

    public List<String> getTokens(String fileName, Reader fileContent) {
        ArrayList<String> tokenList = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(fileContent)) {
            String line;
            while ((line = br.readLine()) != null) {
                tokenList.add(line);
            }
        } catch (IOException e) {
            throw new InternalException("File Error: " + fileName);
        }
        return tokenList;
    }
}
