package mrozowski.silenteight.genderDetector.api;

import mrozowski.silenteight.genderDetector.service.GenderDetectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/detector/gender")
public class GenderDetectorController {
    private final GenderDetectorService service;

    @Autowired
    public GenderDetectorController(GenderDetectorService service) {
        this.service = service;
    }

    @GetMapping(path="/{name}")
    public String detectGender(@PathVariable("name") @NonNull String name){
        return service.detectGender(name);
    }


    @GetMapping(path="/names")
    public List<String> getNames() {
        return service.getNames();
    }
}
