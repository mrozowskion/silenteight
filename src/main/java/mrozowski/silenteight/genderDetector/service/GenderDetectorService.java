package mrozowski.silenteight.genderDetector.service;

import mrozowski.silenteight.genderDetector.dao.DAO;
import mrozowski.silenteight.genderDetector.exception.InvalidNameException;
import mrozowski.silenteight.genderDetector.utils.NameValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

import static mrozowski.silenteight.genderDetector.utils.Constants.*;

@Service
public class GenderDetectorService {
    private final DAO repository;
    private final NameValidator nameValidator;

    @Autowired
    public GenderDetectorService(@Qualifier("file_db") DAO repository, NameValidator nameValidator) {
        this.repository = repository;
        this.nameValidator = nameValidator;
    }

    public String detectGender(String name) {
        String firstName = name.split(" ")[0]; // check only first name
        if(!nameValidator.test(firstName)) throw new InvalidNameException(String.format("Name %s is invalid", firstName));

        if(firstName.endsWith("a")){
            // if name ends with "a" it's more likely it's a female name
            if(repository.isFemaleName(firstName)) return FEMALE;
            return repository.isMaleName(firstName)? MALE : INCONCLUSIVE;
        }
        else{
            if(repository.isMaleName(firstName)) return MALE;
            return repository.isFemaleName(firstName)? FEMALE : INCONCLUSIVE;
        }
    }


    public String detectGenderByNames(String name) {
        int result = 0;
        String [] names = name.split(" ");
        for(String a: names){
            if(detectGender(a).equals(FEMALE)) result -= 1;
            else if(detectGender(a).equals(MALE)) result += 1;
        }

        if(result > 0) return MALE;
        else if(result < 0) return FEMALE;
        else return INCONCLUSIVE;
    }


    public List<String> getNames() {
        // Return list of female & male names
        List<String> list = repository.getFemaleNames();
        list.addAll(repository.getMaleNames());
        return list;
    }
}
