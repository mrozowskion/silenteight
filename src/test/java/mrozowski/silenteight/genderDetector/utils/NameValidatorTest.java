package mrozowski.silenteight.genderDetector.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

public class NameValidatorTest {

    private NameValidator underTest;

    @BeforeEach
    void setUp() {
        underTest = new NameValidator();
    }

    @DisplayName("NameValidator test")
    @ParameterizedTest(name = "\"{0}\" should be {1}")
    @CsvSource({
            "Michał, true",
            "michał, true",
            "MICHAŁ, true",
            "Żółty, true",
            "Magda, true",
            "a, false",
            "mareknlkojknszymondfilksol, false",
            "I2a, false",
            "_mirek, false",
            "' ', false"
    })
    void itShould_ValidateName(String name, boolean expected) {
        // When
        boolean result = underTest.test(name);

        // Then
        assertThat(result).isEqualTo(expected);
    }
}
