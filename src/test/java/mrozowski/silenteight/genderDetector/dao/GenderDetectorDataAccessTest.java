package mrozowski.silenteight.genderDetector.dao;

import com.sun.jdi.InternalException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

class GenderDetectorDataAccessTest {

    private GenderDetectorDataAccess underTest;
    @BeforeEach
    void setUp() {
        underTest = new GenderDetectorDataAccess();
    }

    @Test
    void openFile_ShouldOpenFile_WhenFilePathIsCorrect() {
        // Give
        String path = "/test/daoTestFile.txt";

        // When
        Reader reader = underTest.openFile(path);

        // Then
        assertThat(reader != null).isTrue();
    }

    @Test
    void openFile_ShouldThrowInternalException_WhenFilePathIsWrong() {
        // Given
        String path = "wrong/file/path/name.txt";

        assertThatThrownBy(() -> underTest.openFile(path))
                .isInstanceOf(InternalException.class)
                .hasMessageContaining(String.format("File %s not found", path));
    }

    @Test
    void checkName_ShouldReturnTrue_WhenFindsNameInFile() {
        // Given
        String name = "Magda";
        String namesInRepo = "Olga\nMagda\nJadwiga";

        // When
        boolean isFemaleName = underTest.checkName(name, "femaleNames.txt", new StringReader(namesInRepo));

        // Then
        assertThat(isFemaleName).isTrue();
    }

    @Test
    void checkName_ShouldReturnFalse_WhenCannotFindNameInFile() {
        // Given
        String name = "Rafał";
        String namesInRepo = "Olga\nMagda\nJadwiga";

        // When
        boolean isFemaleName = underTest.checkName(name, "femaleNames.txt", new StringReader(namesInRepo));

        // Then
        assertThat(isFemaleName).isFalse();
    }

    @Test
    void getTokens_ShouldReturnFemaleNames() {
        // Given
        String namesInRepo = "Olga\nMagda\nJadwiga";
        List<String> expected = Arrays.asList("Olga", "Magda", "Jadwiga");

        // When
        List<String> result = underTest.getTokens("femaleNames.txt", new StringReader(namesInRepo));

        // Then
        assertEquals(result, expected);
    }


}