package mrozowski.silenteight.genderDetector.service;

import mrozowski.silenteight.genderDetector.dao.DAO;
import mrozowski.silenteight.genderDetector.exception.InvalidNameException;
import mrozowski.silenteight.genderDetector.utils.NameValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static mrozowski.silenteight.genderDetector.utils.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

class GenderDetectorServiceTest {

    private GenderDetectorService underTest;

    @Mock
    private NameValidator nameValidator;

    @Mock
    private DAO repository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        underTest = new GenderDetectorService(repository, nameValidator);
    }

    @Test
    void detectGender_ShouldReturnMale_WhenGivenNameIsMichal() {
        // Given
        String expected = MALE;
        String name = "Michał";

        // 'Michał' name exists in male repository
        given(repository.isMaleName(name)).willReturn(true);
        given(repository.isFemaleName(name)).willReturn(false);

        // name is valid
        given(nameValidator.test(name)).willReturn(true);

        // When
        String result = underTest.detectGender(name);

        // Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    void detectGender_ShouldReturnFemale_WhenGivenNameIsMagda() {
        // Given
        String expected = FEMALE;
        String name = "Magda";

        // 'Magda' name exists in female repository
        given(repository.isFemaleName(name)).willReturn(true);
        given(repository.isMaleName(name)).willReturn(false);

        // name is valid
        given(nameValidator.test(name)).willReturn(true);

        // When
        String result = underTest.detectGender(name);

        // Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    void detectGender_ShouldReturnFemale_WhenGivenFemaleName_ContainsPolishCharacter() {
        // Given
        String expected = FEMALE;
        String name = "Grażyna";

        // 'Grażyna' name exists in female repository
        given(repository.isFemaleName(name)).willReturn(true);
        given(repository.isMaleName(name)).willReturn(false);

        // name is valid
        given(nameValidator.test(name)).willReturn(true);

        // When
        String result = underTest.detectGender(name);

        // Then
        assertThat(result).isEqualTo(expected);
    }

    @ParameterizedTest(name = "\"{0} should return INCONCLUSIVE")
    @CsvSource({"Rokita", "Burek",})
    void detectGender_ShouldReturnINCONCLUSIVE_WhenGivenValueIsNotName(String name) {
        // Given
        String expected = INCONCLUSIVE;

        // The name does not exist in the repository
        given(repository.isFemaleName(name)).willReturn(false);
        given(repository.isMaleName(name)).willReturn(false);

        // name is valid
        given(nameValidator.test(name)).willReturn(true);

        // When
        String result = underTest.detectGender(name);

        // Then
        assertThat(result).isEqualTo(expected);
    }



    @Test
    void detectGender_ShouldThrowInvalidNameException_WhenNameIsInvalid() {
        // Given
        String invalidName = "Rok1ta";

        // name is valid
        given(nameValidator.test(invalidName)).willReturn(false);

        // Then
        assertThatThrownBy(() -> underTest.detectGender(invalidName))
                .isInstanceOf(InvalidNameException.class)
                .hasMessageContaining(String.format("Name %s is invalid", invalidName));
    }

    @Test
    void detectGender_ShouldReturnMale_WhenGivenValueContainsTwoMaleNames() {
        String expected = MALE;
        String name = "Michał Bogdan";

        // 'Michał' name exists in repository
        given(repository.isMaleName("Michał")).willReturn(true);


        // 'Michał' is valid name
        given(nameValidator.test("Michał")).willReturn(true);

        // When
        String detectedGender = underTest.detectGender(name);

        //Then
        assertThat(detectedGender).isEqualTo(expected);
    }


    @Test
    void detectGenderByNames_ShouldReturnMale_WhenGivenNameEqualsJanMichalRokita() {
        String expected = MALE;
        String name1 = "Jan";
        String name2 = "Michał";
        String name3 = "Rokita";
        String name = name1 + " " + name2 + " " + name3;


        // 'Michał' and 'Jan' names exists in repository
        given(repository.isMaleName(name1)).willReturn(true);
        given(repository.isMaleName(name2)).willReturn(true);

        // 'Rokita' doesnt exist in repository
        given(repository.isFemaleName(name3)).willReturn(false);
        given(repository.isMaleName(name3)).willReturn(false);


        // 'Michał' is valid name
        given(nameValidator.test(name1)).willReturn(true);
        given(nameValidator.test(name2)).willReturn(true);
        given(nameValidator.test(name3)).willReturn(true);

        // When
        String detectedGender = underTest.detectGenderByNames(name);

        //Then
        assertThat(detectedGender).isEqualTo(expected);
    }

    @Test
    void detectGenderByNames_ShouldReturnFemale_WhenGivenNameEqualsAnnaMariaJopek() {
        String expected = FEMALE;
        String name1 = "Anna";
        String name2 = "Maria";
        String name3 = "Jopek";
        String name = name1 + " " + name2 + " " + name3;

        // 'Anna' and 'Maria' names exists in repository
        given(repository.isFemaleName(name1)).willReturn(true);
        given(repository.isFemaleName(name2)).willReturn(true);

        // 'Jopek' doesnt exist in repository
        given(repository.isFemaleName(name3)).willReturn(false);
        given(repository.isMaleName(name3)).willReturn(false);


        // valid name
        given(nameValidator.test(name1)).willReturn(true);
        given(nameValidator.test(name2)).willReturn(true);
        given(nameValidator.test(name3)).willReturn(true);

        // When
        String detectedGender = underTest.detectGenderByNames(name);

        //Then
        assertThat(detectedGender).isEqualTo(expected);
    }

    @Test
    void detectGenderByNames_ShouldReturnINCONCLUSIVE_WhenGivenNameEqualsJanMariaRokita() {
        String expected = INCONCLUSIVE;
        String name1 = "Jan";
        String name2 = "Maria";
        String name3 = "Rokita";
        String name = name1 + " " + name2 + " " + name3;

        // 'Jan' and 'Maria' names exists in repository
        given(repository.isMaleName(name1)).willReturn(true);
        given(repository.isFemaleName(name2)).willReturn(true);

        // 'Rokita' doesnt exist in repository
        given(repository.isFemaleName(name3)).willReturn(false);
        given(repository.isMaleName(name3)).willReturn(false);


        // valid name
        given(nameValidator.test(name1)).willReturn(true);
        given(nameValidator.test(name2)).willReturn(true);
        given(nameValidator.test(name3)).willReturn(true);

        // When
        String detectedGender = underTest.detectGenderByNames(name);

        //Then
        assertThat(detectedGender).isEqualTo(expected);
    }

    @Test
    void detectGenderByNames_ShouldReturnFemale_WhenGivenNameEqualsMaria() {
        String expected = FEMALE;
        String name = "Maria";

        // 'Maria' name exists in repository
        given(repository.isFemaleName(name)).willReturn(true);

        // valid name
        given(nameValidator.test(name)).willReturn(true);

        // When
        String detectedGender = underTest.detectGenderByNames(name);

        //Then
        assertThat(detectedGender).isEqualTo(expected);
    }

    @Test
    void getNames_ShouldReturnListOfFemaleAndMaleNames() {
        // Given
        List<String> femaleNames = new ArrayList<>(Arrays.asList("Olga", "Magda", "Jadwiga"));
        List<String> maleNames = new ArrayList<>(Arrays.asList("Marek", "Edek", "Rafał"));

        List<String> expected = new ArrayList<>(femaleNames);
        expected.addAll(maleNames);

        given(repository.getFemaleNames()).willReturn(femaleNames);
        given(repository.getMaleNames()).willReturn(maleNames);

        //When
        List<String> allTokens = underTest.getNames();

        //Then
        assertEquals(allTokens, expected);
    }


}