package mrozowski.silenteight.genderDetector.api;

import mrozowski.silenteight.genderDetector.service.GenderDetectorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static mrozowski.silenteight.genderDetector.utils.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

class GenderDetectorControllerTest {

    private GenderDetectorController underTest;

    @Mock
    private GenderDetectorService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        underTest = new GenderDetectorController(service);
    }

    @Test
    void detectGender_ShouldReturnMale_WhenGivenNameIsMichal() {
        // Given
        String expected = MALE;
        String name = "Michał";

        given(service.detectGender(name)).willReturn(expected);

        // When
        String gender = underTest.detectGender(name);

        // Then
        assertThat(gender).isEqualTo(expected);
    }

    @Test
    void detectGender_ShouldReturnFemale_WhenGivenNameIsMaria() {
        // Given
        String expected = FEMALE;
        String name = "Maria";

        given(service.detectGender(name)).willReturn(expected);

        // When
        String gender = underTest.detectGender(name);

        // Then
        assertThat(gender).isEqualTo(expected);
    }

    @Test
    void detectGender_ShouldReturnINCONCLUSIVE_WhenGivenNameWasNotFound() {
        // Given
        String expected = INCONCLUSIVE;
        String name = "Rokita";

        given(service.detectGender(name)).willReturn(expected);

        // When
        String gender = underTest.detectGender(name);

        // Then
        assertThat(gender).isEqualTo(expected);
    }

    @Test
    void detectGender_ShouldReturnMale_WhenGivenNameIsJanMariaRokita() {
        // Given
        String expected = MALE;
        String name = "Jan Maria Rokita";

        given(service.detectGender(name)).willReturn(expected);

        // When
        String gender = underTest.detectGender(name);

        // Then
        assertThat(gender).isEqualTo(expected);
    }

    @Test
    void detectGender_ShouldThrowIllegalArgumentException_WhenNameIsInvalid() {
        // Given
        String name = "Maria4";

        given(service.detectGender(name)).willThrow(IllegalArgumentException.class);

        // Then
        assertThrows(IllegalArgumentException.class, () -> underTest.detectGender(name));
    }

    @Test
    void getNames_ShouldReturnListTokens() {
        // Given
        List<String> expected = Arrays.asList("Marek", "Edek", "Rafał");
        given(service.getNames()).willReturn(expected);

        // When
        List<String> tokens = underTest.getNames();

        assertEquals(tokens, expected);
    }


}